/* File used to call functions of XMLReader from another file ("API") */
#ifndef ___include_XML_reader_header_file___
#define ___include_XML_reader_header_file___

#include "dataStructure.h"
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xmlreader.h>

void loadMap(const char *fileToLoad, Map *map);

void loadRoomsForMap(const char *filePath, Map *map);

void loadRoom(const char *fileRoomName, Map *map);

void loadPoints(Piece *p, xmlTextReaderPtr reader);

void loadPointCoordinates(Point *pt, xmlTextReaderPtr reader);

void loadStep(Etape *et, char *id);

void loadStory(Etape *et, xmlTextReaderPtr ptr);

void loadOptions(Etape *et, xmlTextReaderPtr reader);

void loadOption(Option *o, xmlTextReaderPtr reader);

Action* getAction(char *id);

char* getParemeterValueInSave(char *parameter);

#endif