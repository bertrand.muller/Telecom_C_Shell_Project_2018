/* File used to call functions of XMLReader from another file ("API") */
#ifndef ___include_XML_writer_header_file___
#define ___include_XML_writer_header_file___

#include "dataStructure.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xmlreader.h>

void saveData(char *dataTitles[], char *dataValues[], int numberOfData);

#endif