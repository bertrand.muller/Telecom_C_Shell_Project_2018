/* File used to share init functions between all files of the application */
#ifndef ___include_init_func_header_file___
#define ___include_init_func_header_file___

#include "dataStructure.h"

Option* initOption();

Etape* initEtape();

Etape* initEtapeFromSave();

Personnage* initPersonnage(char nom[11]);

Personnage* initPersonnageFromSave();

Piece* initPiece();

Point* initPoint();

Map* initMap();

Log* initLogFile();

Action* initAction();

#endif
