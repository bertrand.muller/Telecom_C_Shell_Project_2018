/* File used to share the same data structure between all files of the application */
#ifndef ___include_structs_header_file___
#define ___include_structs_header_file___

#define MAX_NB_OPTIONS 5
#define MAX_NB_POINTS 15
#define MAX_NB_PIECES 20
#define LARGEUR_MAP 50
#define HAUTEUR_MAP 30
#define TAILLE_MAXI_LIGNE_TEXTE 1023
#define MAX_DATA_TO_SAVE 20
#define MAX_NUMBER_PARAMETERS_ACTION 10

#define max(x,y) ((x) >= (y)) ? (x) : (y)
#define min(x,y) ((x) <= (y)) ? (x) : (y)

typedef struct Point Point;
typedef struct Piece Piece;
typedef struct Map Map;
typedef struct Personnage Personnage;
typedef struct Option Option;
typedef struct Etape Etape;
typedef struct Log Log;
typedef struct Action Action;

struct Point {
    int x;
    int y;
};

struct Piece {
    char *idEtape;
    char *idPiece;
    int visible;
    int nbPoints;
    Point *points[MAX_NB_POINTS];
};


struct Map {
    int affichage[LARGEUR_MAP][HAUTEUR_MAP];
    int nbPieces;
    Piece* pieces[MAX_NB_PIECES];
};

struct Personnage {
	char nom[11];
    int vie;
    int humanite;
    int dexterite;
    int vigueur;
    int perception;
    int intelligence;
};

struct Option {
	char *titre;
    char *texte;
	char *echec;
	char *reussite;
    char *proEtapeSucces;
	char *proEtapeEchec;
    int dureeSucces;
    int dureeEchec;
    int vieSucces;
    int vieEchec;
    int humaniteSucces;
    int humaniteEchec;
    char *action;
};

struct Etape {
    char *idPiece;
    char *texte;
    int nbOptions;
    Option *options[MAX_NB_OPTIONS];
};

struct Log {
    FILE* file;
    int ligneActuelle;
    int rangLigneActuelle;
    int rangLigneMax;
    int rangAAjouter;
    int afficherLigneEntiere;
    int maxLigne;
    int nouveautes;
};

struct Action {
    char *nomMethode;
    int nbParametres;
    char *parametres[MAX_NUMBER_PARAMETERS_ACTION];
};

#endif
