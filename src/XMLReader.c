#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "XMLReader.h"
#include "initFunctions.h"

/**
 * Used to get the name of the pointed node
 * @param reader Pointer to an XML node
 * @return Name of the node the pointer is pointing to
 */
char * getNodeName(xmlTextReaderPtr reader) {
    return (char *) xmlTextReaderConstName(reader);
}


/**
 * Used to get the node type of the pointed node
 * @param reader Pointer to an XML node
 * @return Type of the node identified by an integer value
 */
int getNodeType(xmlTextReaderPtr reader) {
    return xmlTextReaderNodeType(reader);
}


/**
 * Used to get the node value of the pointed node
 * @param reader Pointer to an XML node
 * @return Value of the node in string format
 */
char* getNodeValue(xmlTextReaderPtr reader) {
    char *value = (char *) xmlTextReaderConstValue(reader);
    if(value == NULL) {
        return "";
    }
    return value;
}


/**
 * Used to get a node property (attribute) of the pointed node
 * @param node Node retrieved from the pointer
 * @param property Property we're looking for
 * @return Value of the property in string format
 */
char* getNodeProperty(xmlNodePtr node, xmlChar *property) {
    return (char *) xmlGetProp(node, property);
}


/**
 * Used to load the map, from an XML file, by parsing the list of rooms
 * @param fileToLoad Name of the file to load & to parse
 * @param map Pointer to a map which will be set
 */
void loadMap(const char *fileToLoad, Map *map) {
    // Build XML file path
    char *filePath = malloc(strlen("xml/maps/") + strlen(fileToLoad) + strlen(".xml") + 1);
    if(filePath == NULL) { exit(127); }
    strcpy(filePath, "xml/maps/");
    strcat(filePath, fileToLoad);
    strcat(filePath, ".xml");

    // Check if file exists or not
    if(access(filePath, F_OK) == -1) {
        printf("Error while loading map. File './%s' does not exist.\n", filePath);
        exit(127);
    }

    // Get rooms linked to this map
    loadRoomsForMap(filePath, map);

}


/**
 * Used to load rooms, from multiple XML files, to populate the map
 * @param filePath Path of the file to load & to parse the rooms
 * @param map Pointer to a map
 */
void loadRoomsForMap(const char *filePath, Map *map) {

    int read, nodeType;
    char *name, *value;
    xmlTextReaderPtr reader = xmlReaderForFile(filePath, NULL, 0);

    if(reader != NULL) {
        read = xmlTextReaderRead(reader);

        while(read == 1) {
            name = getNodeName(reader);
            nodeType = getNodeType(reader);
            if(nodeType == 1) {
                if(strcmp(name, "Room") == 0) {
                    while(nodeType != 3) {
                        read = xmlTextReaderRead(reader);
                        nodeType = getNodeType(reader);
                    }
                    value = getNodeValue(reader);
                    loadRoom(value, map);
                }
            }
            read = xmlTextReaderRead(reader);
        }

        xmlFreeTextReader(reader);
        if(read != 0) {
            printf("Error while parsing './%s' file.\n", filePath);
        }

    } else {
        printf("Error while starting to parse './%s' file.\n", filePath);
        exit(127);
    }

}


/**
 * Used to load a room, from an XML file, to populate the rooms of a map
 * @param fileRoomName Name of the file to load & to parse for a room
 * @param map Pointer to a map
 */
void loadRoom(const char *fileRoomName, Map *map) {

    // Build XML file path
    char *filePath = malloc(strlen("xml/rooms/") + strlen(fileRoomName) + strlen(".xml") + 1);
    if(filePath == NULL) { exit(127); }
    strcpy(filePath, "xml/rooms/");
    strcat(filePath, fileRoomName);
    strcat(filePath, ".xml");

    // Check if file exists or not
    if(access(filePath, F_OK) == -1) {
        printf("Error while loading map. File './%s' does not exist.\n", filePath);
        exit(127);
    }

    int read, nodeType;
    char *name, *visible, *step;
    xmlTextReaderPtr reader = xmlReaderForFile(filePath, NULL, 0);
    xmlNodePtr node;
    xmlChar *attr;
    Piece *p = initPiece();

    // Get data for the room
    if(reader != NULL) {

        read = xmlTextReaderRead(reader);

        while(read == 1) {
            name = getNodeName(reader);
            nodeType = getNodeType(reader);
            if(nodeType == 1) {
                if(strcmp(name, "Room") == 0) {
                    node = xmlTextReaderCurrentNode(reader);
                    attr = xmlCharStrdup("visible");
                    visible = getNodeProperty(node,attr);
                    p->visible = atoi(visible);
                    attr = xmlCharStrdup("step");
                    step = getNodeProperty(node,attr);
                    p->idEtape = step;
                    p->idPiece = malloc(strlen(fileRoomName)+1);
    					  strcpy(p->idPiece, fileRoomName);
                } else if(strcmp(name, "Points") == 0) {
                    loadPoints(p, reader);
                    map->pieces[map->nbPieces] = p;
                    map->nbPieces++;
                }
            }
            read = xmlTextReaderRead(reader);
        }

        xmlFreeTextReader(reader);
        if(read != 0) {
            printf("Error while parsing './%s' file.\n", filePath);
        }

    } else {
        printf("Error while starting to parse './%s' file.\n", filePath);
        exit(127);
    }
}


/**
 * Used to get points wich are used to display a room in the console
 * @param p Pointer to a room
 * @param reader Pointer to an XML node
 */
void loadPoints(Piece *p, xmlTextReaderPtr reader) {

    int read, nodeType;
    char *name;

    read = xmlTextReaderRead(reader);
    read = xmlTextReaderRead(reader);

    while(read == 1) {
        name = getNodeName(reader);
        nodeType = getNodeType(reader);
        if((nodeType == 1) && (strcmp(name, "Point") == 0)) {
            Point *pt = initPoint();
            loadPointCoordinates(pt, reader);
            p->points[p->nbPoints] = pt;
            p->nbPoints++;
        }
        read = xmlTextReaderRead(reader);
    }

}


/**
 * Used to get the coordinates of a point
 * @param pt Pointer to a point
 * @param reader Pointer to an XML node
 */
void loadPointCoordinates(Point *pt, xmlTextReaderPtr reader) {

    char *name, *value;

    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    name = getNodeName(reader);

    if(strcmp(name, "x") == 0) {
        xmlTextReaderRead(reader);
        value = getNodeValue(reader);
        pt->x = atoi(value);
    }

    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    name = getNodeName(reader);

    if(strcmp(name, "y") == 0) {
        xmlTextReaderRead(reader);
        value = getNodeValue(reader);
        pt->y = atoi(value);
    }

}


/**
 * Used to get the step we're looking for in the game
 * @param et Pointer to a step
 * @param id String value to identify what step has to be loaded
 */
void loadStep(Etape *et, char *id) {

    // Build XML file path
    char *filePath = malloc(strlen("xml/steps/step") + strlen(id) + strlen(".xml") + 1);
    if(filePath == NULL) { exit(127); }
    strcpy(filePath, "xml/steps/step");
    strcat(filePath, id);
    strcat(filePath, ".xml");

    // Check if file exists or not
    if(access(filePath, F_OK) == -1) {
        printf("Error while loading step. File './%s' does not exist.\n", filePath);
        exit(127);
    }

    // Get story and options for this step
    xmlTextReaderPtr reader = xmlReaderForFile(filePath, NULL, 0);
    loadStory(et, reader);
    loadOptions(et, reader);

}


/**
 * Used to get a story associated to a step
 * @param et Pointer to a step
 * @param reader Pointer to an XML node
 */
void loadStory(Etape *et, xmlTextReaderPtr reader) {

    int read, nodeType;
    char *name, *value;
    xmlNodePtr node;
    xmlChar *attr;

    read = xmlTextReaderRead(reader);
    name = getNodeName(reader);
    nodeType = getNodeType(reader);

    while((read == 1) && (strcmp(name, "Options") != 0)) {

        if((nodeType == 1) && (strcmp(name, "Step") == 0)) {

            node = xmlTextReaderCurrentNode(reader);
            attr = xmlCharStrdup("room");
            et->idPiece = getNodeProperty(node,attr);

            read = xmlTextReaderRead(reader);
            read = xmlTextReaderRead(reader);
            name = getNodeName(reader);
            nodeType = getNodeType(reader);
            if((nodeType == 1) && (strcmp(name, "Story") == 0)) {
                read = xmlTextReaderRead(reader);
                value = getNodeValue(reader);
                et->texte = malloc(strlen(value) + 1);
                strcpy(et->texte, value);
            }

        }

        read = xmlTextReaderRead(reader);
        name = getNodeName(reader);
        nodeType = getNodeType(reader);

    }

}


/**
 * Used to get options associated to a step's story
 * @param et Pointer to a step
 * @param reader Pointer to an XML node
 */
void loadOptions(Etape *et, xmlTextReaderPtr reader) {

    int read, nodeType;
    char *name;

    read = xmlTextReaderRead(reader);
    while(read == 1) {

        name = getNodeName(reader);
        nodeType = getNodeType(reader);

        if((nodeType == 1) && (strcmp(name,"Option") == 0)) {
            Option *o = initOption();
            loadOption(o, reader);
            et->options[et->nbOptions] = o;
            et->nbOptions++;
        }

        read = xmlTextReaderRead(reader);

   }
}


/**
 * Used to get an option associated to a step's story
 * @param o Pointer to an option
 * @param reader Pointer to an XML node
 */
void loadOption(Option *o, xmlTextReaderPtr reader) {

    int nodeType;
    char *name, *value;
    xmlNodePtr node;
    xmlChar *attr;

    // Get the title
    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    name = getNodeName(reader);
    nodeType = getNodeType(reader);

    if((strcmp(name, "Title") == 0) && (nodeType == 1)) {
        xmlTextReaderRead(reader);
        value = getNodeValue(reader);
        o->titre = malloc(sizeof(char)*(strlen(value) + 1));
        strcpy(o->titre, value);
    }

    // Get the text
    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    name = getNodeName(reader);
    nodeType = getNodeType(reader);
    if((strcmp(name, "Text") == 0) && (nodeType == 1)) {
        xmlTextReaderRead(reader);
        value = getNodeValue(reader);
        o->texte = malloc(sizeof(char)*(strlen(value) + 1));
        strcpy(o->texte, value);
    }

    // Get the action to execute
    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    name = getNodeName(reader);
    nodeType = getNodeType(reader);
    if(strcmp(name, "Action") == 0) {
        node = xmlTextReaderCurrentNode(reader);
        attr = xmlCharStrdup("do");
        value = getNodeProperty(node, attr);
        o->action = malloc(sizeof(char)*(strlen(value) + 1));
        strcpy(o->action, value);
    }

    // Get the next step for success
    xmlTextReaderRead(reader);
    xmlTextReaderRead(reader);
    name = getNodeName(reader);
    nodeType = getNodeType(reader);
    if(strcmp(name, "NextSuccess") == 0) {
        node = xmlTextReaderCurrentNode(reader);
        attr = xmlCharStrdup("to");
        value = getNodeProperty(node, attr);
        o->proEtapeSucces = malloc(sizeof(char)*(strlen(value) + 1));
        strcpy(o->proEtapeSucces, value);
        attr = xmlCharStrdup("duration");
        o->dureeSucces = atoi(getNodeProperty(node,attr));
        attr = xmlCharStrdup("vie");
        o->vieSucces = atoi(getNodeProperty(node,attr));
        attr = xmlCharStrdup("humanite");
        o->humaniteSucces = atoi(getNodeProperty(node,attr));
        xmlTextReaderRead(reader);
        value = getNodeValue(reader);
        o->reussite = malloc(sizeof(char)*(strlen(value) + 1));
        strcpy(o->reussite, value);
    }

    // Get the next step for fail
     xmlTextReaderRead(reader);
     xmlTextReaderRead(reader);
     xmlTextReaderRead(reader);
     name = getNodeName(reader);
     nodeType = getNodeType(reader);
     if(strcmp(name, "NextFail") == 0) {
         node = xmlTextReaderCurrentNode(reader);
         attr = xmlCharStrdup("to");
         value = getNodeProperty(node, attr);
         //o->proEtapeEchec = value;
         o->proEtapeEchec = malloc(sizeof(char)*(strlen(value) + 1));
         strcpy(o->proEtapeEchec, value);
         attr = xmlCharStrdup("duration");
         o->dureeEchec = atoi(getNodeProperty(node,attr));
         attr = xmlCharStrdup("vie");
         o->vieEchec = atoi(getNodeProperty(node,attr));
         attr = xmlCharStrdup("humanite");
         o->humaniteEchec = atoi(getNodeProperty(node,attr));
         xmlTextReaderRead(reader);
         value = getNodeValue(reader);
        // o->echec = value;
         o->echec = malloc(sizeof(char)*(strlen(value) + 1));
         strcpy(o->echec, value);
  	} else {
         o->proEtapeEchec = malloc(sizeof(char)*(strlen(o->proEtapeSucces) + 1));
         strcpy(o->proEtapeEchec, o->proEtapeSucces);
         o->echec = malloc(sizeof(char)*(strlen(o->reussite) + 1));
         strcpy(o->echec, o->reussite);
        //o->proEtapeEchec = o->proEtapeSucces;
        //o->echec = o->reussite;
        o->dureeEchec = o->dureeSucces;
        o->vieEchec = o->vieSucces;
        o->humaniteEchec = o->humaniteSucces;
    }

}


/**
 * Used to get the action triggered by user's choice
 * @param id String value to identify which action we wanna retrieve
 * @return Struct of an action
 */
Action* getAction(char *id) {
    char *parameter;

    // Build XML file path
    char *filePath = malloc(strlen("xml/actions/actions.xml") + 1);
    if(filePath == NULL) { exit(127); }
    strcpy(filePath, "xml/actions/actions.xml");

    // Check if file exists or not
    if(access(filePath, F_OK) == -1) {
        printf("Error while getting action. File './%s' does not exist.\n", filePath);
        exit(127);
    }

    // Get the action we're looking for
    Action *a = initAction();
    xmlTextReaderPtr reader = xmlReaderForFile(filePath, NULL, 0);
    int read = xmlTextReaderRead(reader);
    int nodeType;
    char *name, *value = "", *idAction;
    xmlNodePtr node;
    xmlChar *attr = xmlCharStrdup("id");
    while(read == 1) {

        name = getNodeName(reader);
        nodeType = getNodeType(reader);
        node = xmlTextReaderCurrentNode(reader);
        idAction = getNodeProperty(node, attr);
        if((nodeType == 1) && (strcmp(name, "Action") == 0) && (strcmp(idAction,id) == 0)) {
            read = xmlTextReaderRead(reader);
            value = getNodeValue(reader);
            a->nomMethode = strtok(value, ":");
            parameter = strtok(NULL, ":");
            a->nbParametres = 0;
            while(parameter != NULL) {
                a->parametres[a->nbParametres] = malloc(strlen(parameter) + 1);
                strcpy(a->parametres[a->nbParametres], parameter);
                parameter = strtok(NULL, ":");
                a->nbParametres++;
            }
            return a;
        }

        read = xmlTextReaderRead(reader);

    }

    return a;

}


/**
 * Used to get the value of a parameter saved in the save file
 * @param parameter Parameter to retrieve from the save file
 * @return The value associated to the parameter
 */
char* getParemeterValueInSave(char *parameter) {

    // Build XML file path
    char *filePath = malloc(strlen("xml/saves/save.xml") + 1);
    if(filePath == NULL) { exit(127); }
    strcpy(filePath, "xml/saves/save.xml");

    // Check if file exists or not
    if(access(filePath, F_OK) == -1) {
        printf("Error while getting action. File './%s' does not exist.\n", filePath);
        exit(127);
    }

    // Get value of the paramter we're looking for
    xmlTextReaderPtr reader = xmlReaderForFile(filePath, NULL, 0);
    int read = xmlTextReaderRead(reader);
    int nodeType;
    char *name, *value = "";

    while(read == 1) {

        name = getNodeName(reader);
        nodeType = getNodeType(reader);

        if((nodeType == 1) && (strcmp(name, parameter) == 0)) {
            read = xmlTextReaderRead(reader);
            value = getNodeValue(reader);
            return value;
        }

        read = xmlTextReaderRead(reader);

    }

    return value;

}
