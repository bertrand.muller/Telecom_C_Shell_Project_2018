#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "initFunctions.h"
#include "XMLReader.h"


/**
 * Used to initialize an option for the story
 * @return Option with attributes initialized with their default value
 */
Option* initOption(){
    Option* o = malloc(sizeof(Option));
    if(o == NULL){exit(127);}
    o->action = "1";
    o->proEtapeSucces = "1";
    o->proEtapeEchec = "1";
    o->texte = "";
    o->titre = "";
    o->reussite = "";
    o->echec = "";
    o->dureeSucces = 0;
    o->dureeEchec = 0;
    o->humaniteSucces = 0;
    o->humaniteEchec = 0;
    o->vieSucces = 0;
    o->vieEchec = 0;
    return o;
}


/**
 * Used to initialize a step for the story
 * @return Step with attributes initialized with their default value
 */
Etape* initEtape(){
    Etape* e = malloc(sizeof(Etape));
    if(e == NULL){exit(127);}
    e->texte = "";
    e->nbOptions = 0;
    e->idPiece = "1";
    return e;
}


/**
 * Used to initialize a step from a save file
 * @return Step with attributes initialized with last values saved
 */
Etape* initEtapeFromSave(){
    Etape* e = malloc(sizeof(Etape));
    if(e == NULL){exit(127);}
    e->idPiece = getParemeterValueInSave("S.id");
    return e;
}


/**
 * Used to initialize a room for the story
 * @return Room with attributes initialized with their default value
 */
Piece* initPiece(){
	Piece* p = malloc(sizeof(Piece));
    if(p == NULL) { exit(127); }
    p->visible = 0;
    p->nbPoints = 0;
    for(int i = 0; i < MAX_NB_POINTS; i++) {
        p->points[i] = initPoint();
    }
    return p;
}


/**
 * Used to initialize a point for the map
 * @return Point with attributes initialized with their default value
 */
Point* initPoint(int x, int y){
    Point* p = malloc(sizeof(Point));
    if(p == NULL){exit(127);}
    p->x = 0;
    p->y = 0;
    return p;
}


/**
 * Used to initialize a character to start the game
 * @return Character with attributes initialized with their default value
 */
Personnage* initPersonnage(char nom[11]){
    Personnage* p = malloc(sizeof(Personnage));
    if(p == NULL){exit(127);}
    strncpy(p->nom, nom, 11);
    return p;
}


/**
 * Used to initialize a character from a save file
 * @return Character with attributes initialized with last values ssaved
 */
Personnage * initPersonnageFromSave() {
    Personnage *p = malloc(sizeof(Personnage));
    if(p == NULL) { exit(127); }
    p->vie = atoi(getParemeterValueInSave("P.vie"));
    p->humanite = atoi(getParemeterValueInSave("P.humanite"));
    p->dexterite = atoi(getParemeterValueInSave("P.dexterite"));
    p->vigueur = atoi(getParemeterValueInSave("P.vigueur"));
    p->perception = atoi(getParemeterValueInSave("P.perception"));
    p->intelligence = atoi(getParemeterValueInSave("P.intelligence"));
    strcpy(p->nom, getParemeterValueInSave("P.nom"));
    return p;
}


/**
 * Used to initialize a map to start the game
 * @return Map with attributes initialized with their default value
 */
Map* initMap(){
    Map *m = malloc(sizeof(Map));
    if(m == NULL){exit(127);}
    m->nbPieces = 0;
    return m;
}


/**
 * Used to initialize a log file to show text to the gamer
 * @return Log with attributes initialized with their default value
 */
Log* initLogFile(){
    Log* l = malloc(sizeof(Log));
    if(l == NULL){exit(127);}
    l->file = fopen("log.txt", "a+");
    l->maxLigne = 0;
    char ligne[TAILLE_MAXI_LIGNE_TEXTE];
    while (fgets(ligne, sizeof(ligne), l->file)) {
        l->maxLigne = l->maxLigne+1;
    }
    l->ligneActuelle = l->maxLigne;
    l->rangLigneActuelle = -1;
    l->rangAAjouter = 0;
    l->afficherLigneEntiere = 1;
    l->nouveautes = 0;
    return l;
}


/**
 * Used to initialize an action for an option chosen by the user
 * @return  Action with attributes initialized with their default value
 */
Action* initAction() {
    Action *a = malloc(sizeof(Action));
    if(a == NULL){exit(127);}
    a->nomMethode = "noaction";
    a->nbParametres = 0;
    return a;
}
