/* File used to share a toolbox between all files of the application */
#ifndef ___include_toolbox_header_file___
#define ___include_toolbox_header_file___

char* intToStr(int nb);

char* getTime();

#endif
