
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include <ncurses.h>
#include <curses.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include "XMLReader.h"
#include "XMLWriter.h"
#include "initFunctions.h"
#include "toolbox.h"

// Dimensions
#define MIN_COLS 40
#define MIN_LINES 30


// Colors
#define BLANC_SUR_NOIR 1
#define NOIR_SUR_BLANC 2
#define JAUNE_SUR_NOIR 3
#define BLANC_SUR_ROUGE 4
#define NOIR_SUR_TURQUOISE 5
#define BRUN_SUR_NOIR 6
#define ROUGE_SUR_BLANC 7
#define ROUGE_SUR_NOIR 8
#define GRIS_SUR_BLANC 9
#define GRIS_SUR_NOIR 10
#define GRIS_FONCE_SUR_NOIR 14
#define VERT_SUR_BLANC 11
#define VERT_SUR_NOIR 12
#define NOIR_SUR_GRIS 13


// Status -> Color
#define STATUS_INFO NOIR_SUR_BLANC
#define STATUS_ERREUR BLANC_SUR_ROUGE
#define STATUS_TODO NOIR_SUR_TURQUOISE
#define STATUS_NON_LU NOIR_SUR_GRIS


// List of the walls according to [TOP][BOTTOM][LEFT][RIGHT]
char TAB_MURS[2][2][2][2][7] = {
		{ // Nothing at the top
				{ // Nothing at the bottom
						{"\u25AA","\u257A"},
						{"\u2578","\u2501"}
				},
				{ // Something at the bottom
						{"\u257B","\u250F"},
						{"\u2513","\u2533"}
				}
		},
		{ // Something at the top
				{ // Nothing at the bottom
						{"\u2579","\u2517"},
						{"\u251B","\u253B"}
				},
				{ // Something at the bottom
						{"\u2503","\u2523"},
						{"\u252B","\u254B"}
				}
		}
};


/**
 * Used to display a string
 * @param lines Number of lines
 * @param cols Number of columns
 * @param string String to display
 * @param color Color used to render the string
 */
void afficher(int lines, int cols, char *string, int color){
    attron (COLOR_PAIR (color));
    mvprintw(lines,cols,string);
    attroff (COLOR_PAIR (color));
}


/**
 * Used to display information
 * @param string String to display
 * @param status Status mode
 */
void afficherInformation(char *string, int status){
	for(int i=0; i<COLS-26; i++){mvprintw(LINES-1,i," ");}
    attron (COLOR_PAIR (status));
    int cols = (COLS-strlen(string))/2;
    mvprintw(LINES-1,cols,string);
    attroff (COLOR_PAIR (status));
}


/**
 * Used to save logs in the log file (on the disk)
 * @param l Log structure
 */
void saveLog(Log *l){
    off_t position = ftell(l->file);
    fclose(l->file);
    l->file = fopen("log.txt", "a+");
    fseek(l->file,position,SEEK_SET);
}


/**
 * Used to add logs and to save them in the specified file
 * @param l Log structure
 * @param str String to write in the logs
 * @param style String style (title, option, action ...)
 */
void ecrireDansLog(Log *l, char* str, char style){

	if(strlen(str) > 1){
		if(str[strlen(str)-1] == '\n'){
			char *tmp = malloc(sizeof(char)*(strlen(str)));
			for(int i=0; i<strlen(str)-1;i++){tmp[i] = str[i];}
			str = tmp;
		}
	}

	bool estSuite = (style == 'f' || style == 's');
    int nbLignes = 0;
    int pos = (estSuite?0:1);
    char *moitie = malloc(sizeof(char)*(strlen(str)+pos+1));
    bool aff;
    if(!estSuite){moitie[0] = style;}
    int debut = (str[0] == '\n'?1:0);

    for(int i=debut; i<strlen(str); i++){
    	aff = true;
    	moitie[pos] = str[i];
 		pos++;
    	if(str[i] == '\n'){	
    		aff = false;
    		moitie[pos] = '\0';
    		fputs(moitie, l->file);
    		pos = 1;
    		moitie = malloc(sizeof(char)*(strlen(str)+2));
    		if(!estSuite){moitie[0] = style;}
    		nbLignes++;
    	}
    }

	if(aff){
		moitie[pos] = '\0';
		fputs(moitie, l->file);
	}

	switch(style){
		case 'h':
		case 'o':
		case 'g':
		case 'v':
			fputs("\n", l->file);
			nbLignes++;
		case 't':
		case 'r':
		case 'e':
		case 'f':
			fputs("\n", l->file);
			nbLignes++;
			break;
		case 'd':
		case 's':
			break;
	}

    l->maxLigne = l->maxLigne+nbLignes;
    l->nouveautes = 1;
    afficher(LINES-1,COLS-24,"\u21AF",JAUNE_SUR_NOIR);
    saveLog(l);

}


/**
 * Used to get a string in the right format
 * @param chaine String to format
 * @return New string with the expected format
 */
char* getChaine(char *chaine){
	char *ligne = malloc(sizeof(char)*(strlen(chaine)));
	for(int i=0;i<strlen(chaine)-1;i++){
		*(ligne+i) = *(chaine+i);
	}
	*(ligne+strlen(chaine)-1) = '\0';
	return ligne;
}


/**
 * Used to get a specific line from the logs
 * @param l Log structure
 * @param ligneALire Line number
 * @return Value of the line according to the line number
 */
char* getLigne(Log l, int ligneALire){
	fseek(l.file,0,SEEK_SET);
	char *ligne = malloc(sizeof(char)*TAILLE_MAXI_LIGNE_TEXTE);
	int i=1;
	while (fgets(ligne, TAILLE_MAXI_LIGNE_TEXTE, l.file)) {
		l.ligneActuelle = i;
		if(i == ligneALire){
			return getChaine(ligne);
		}
		i++;
   }
	return "";
}


/**
 * Used to get the next line of the logs
 * @param l Log structure
 * @return Next line according to the current line number
 */
char* getLigneSuivante(Log l){
	char *ligne = malloc(sizeof(char)*TAILLE_MAXI_LIGNE_TEXTE);
	fgets(ligne, TAILLE_MAXI_LIGNE_TEXTE, l.file);
	l.ligneActuelle++;
	return getChaine(ligne);
}


/**
 * Used to get the previous line of the logs
 * @param l Log structure
 * @return Previous line according to the current line number
 */
char* getLignePrecedente(Log l){
	return getLigne(l,l.ligneActuelle-1);
}


char TAB_CURSEUR[8][7] = {
	"\u2581",
	"\u2582",
	"\u2583",
	"\u2584",
	"\u2585",
	"\u2586",
	"\u2587",
	"\u2588"
};


/**
 * Used to get a part of a string
 * @param chaine String to get a subpart of
 * @param debut Start
 * @param quantite Quantity
 * @return The subpart of the string
 */
char* getPartieChaine(char *chaine, int debut, int quantite){
	char *ligne = malloc(sizeof(char)*(quantite+1));
	for(int i=0;i<quantite;i++){
		*(ligne+i) = *(chaine+i+debut);
	}
	*(ligne+quantite) = '\0';
	return ligne;
}


/**
 * Used to get a line from the display
 * @param ligne Line
 * @param lignes Lines
 * @param decallage Offset
 * @return
 */
int getLigneAffichage(char *ligne, char **lignes,int decallage){

	int lignesAAfficher = 0;
	int i = COLS-23-decallage;
	int debut = 1;
	if(strlen(ligne) < 1){
		lignes[lignesAAfficher] = "";
		return 1;
	}
	
	while(i >= 0 && i < strlen(ligne)){
		if(ligne[i] == ' '){
			lignes[lignesAAfficher] = getPartieChaine(ligne,debut,i-debut);
			lignesAAfficher = lignesAAfficher+1;
			debut = i+1;
			i += COLS-22-decallage;
		}else{
			i--;
		}
	}

	lignes[lignesAAfficher] = getPartieChaine(ligne,debut,strlen(ligne)-debut);
	return lignesAAfficher+1;

}


/**
 *
 * @param l
 */
void afficherTexte(Log *l){

	// Text already printed is removed
	for(int i=15;i>0;i--){
		for(int h=0;h<COLS-23;h++){
			afficher(LINES-i,h," ",BLANC_SUR_NOIR);
		}
	}

	// Number of strings in one sentence (if it is bigger than the window size)
	int nombreChaines;

	// Lines to display
	char **lignes = (char**) malloc((14)*sizeof(char*));

	int i=1;
	int ligneNumero=1;
	char *ligne;
	int position = l->ligneActuelle+1;
	int nb;
	int premierPassage = 1;
	int style;
	int decallage;

	// Display the text in the bottom window
	while(i <= 14){

		ligne = getLigne(*l,position-ligneNumero);
		style = BLANC_SUR_NOIR;
		decallage = 0;
		switch(ligne[0]){
			case 't':
				style = JAUNE_SUR_NOIR;
				break;
			case 'h':
				style = BLANC_SUR_NOIR;
				break;
			case 'g':
				style = ROUGE_SUR_BLANC;
				break;
			case 'v':
				style = VERT_SUR_BLANC;
				break;
			case 'r':
				style = VERT_SUR_NOIR;
				decallage = 1;
				break;
			case 'd':
				style = GRIS_SUR_NOIR;
				decallage = 1;
				break;
			case 'e':
				style = ROUGE_SUR_NOIR;
				decallage = 1;
				break;
			case 'o':
				style = GRIS_SUR_NOIR;
				decallage = 2;
				break;
		}

		nombreChaines = getLigneAffichage(ligne,lignes,decallage);

		if(premierPassage == 1){
			premierPassage = 0;
			if(l->afficherLigneEntiere == 1){
				nb = nombreChaines-1;
			}else{
				nb = l->rangLigneActuelle+l->rangAAjouter;
				if(nb < 0){
					l->rangAAjouter = 0;
					l->ligneActuelle = l->ligneActuelle-1;
					ligne = getLigne(*l,l->ligneActuelle-ligneNumero);
					nombreChaines = getLigneAffichage(ligne,lignes,decallage);
					nb = nombreChaines-1;
				}else if(nb > nombreChaines-1){
					l->rangAAjouter = 0;
					l->ligneActuelle = l->ligneActuelle+1;
					ligne = getLigne(*l,l->ligneActuelle-ligneNumero);
					nombreChaines = getLigneAffichage(ligne,lignes,decallage);
					nb = nombreChaines-1;
				}
			}
			l->rangLigneActuelle = nb;
			l->rangLigneMax = nombreChaines-1;
			l->rangAAjouter = 0;
			l->afficherLigneEntiere = 0;
		}else{
			nb=nombreChaines-1;		
		}
		
		while(nb >= 0 && i <= 14){
			afficher(LINES-1-i,1+decallage,lignes[nb],style);
			nb--;
			i++;
		}

		ligneNumero++;

	}
	
	// Display the scroll bar
	if((l->ligneActuelle > 1) || (l->ligneActuelle == 1 && l->rangLigneActuelle > 0)){ afficher(LINES-15,COLS-24,"\u2191",BLANC_SUR_NOIR); }
	else{ afficher(LINES-15,COLS-24," ",BLANC_SUR_NOIR); }
	
	if(l->ligneActuelle < l->maxLigne || ( l->ligneActuelle == l->maxLigne && l->rangLigneActuelle < l->rangLigneMax)){
		if(l->nouveautes == 1){ afficher(LINES-1,COLS-24,"\u21AF",JAUNE_SUR_NOIR); }
		else{ afficher(LINES-1,COLS-24,"\u2193",BLANC_SUR_NOIR); };
	} else{
		afficher(LINES-1,COLS-24," ",BLANC_SUR_NOIR);
		l->nouveautes = 0;
	}

	int division = ((float)(l->ligneActuelle)/l->maxLigne*8*13);
	int carre = 0;
	while(division > 8){
		carre++;
		division = division-8;
	}

	if(carre > 0){
		if(division == 8){
			afficher(LINES-14+carre,COLS-24,TAB_CURSEUR[7],BLANC_SUR_NOIR);
		}else{
			afficher(LINES-14+carre-1,COLS-24,TAB_CURSEUR[7-division],BLANC_SUR_NOIR);
			afficher(LINES-14+carre,COLS-24,TAB_CURSEUR[7-division],NOIR_SUR_BLANC);
		}
	}else{
		afficher(LINES-14,COLS-24,TAB_CURSEUR[7],BLANC_SUR_NOIR);
	}

	if(l->nouveautes != 0){
		afficherInformation(" TEXTE NON LU CI-DESSOUS ",STATUS_NON_LU);
	}else{
		afficherInformation("",STATUS_INFO);
	}

}


/**
 * Used to go to the end of the text window
 * @param l Log structure
 */
void allerFinTexte(Log *l){
	l->ligneActuelle = l->maxLigne; 
	l->afficherLigneEntiere = 1;
	afficherTexte(l);
}


char DES[10][10] = {"\u2460","\u2461","\u2462","\u2463","\u2464","\u2465","\u2466","\u2467","\u2468","\u2469"};


/**
 * Used to simulate dice roll
 * @param stat Character attribute being eventually affected by the simulation
 * @param difficulte Difficulty of the simulation
 * @param l Log structure
 * @param p Character structure
 * @return Boolean which indicates the success or not
 */
bool jetDeDes(char* stat, int difficulte, Log *l, Personnage p){

	int nbDes = 0; 
	if(strcmp(stat,"dexterite") == 0){
		nbDes = p.dexterite;
	}else if(strcmp(stat,"vigueur") == 0){
		nbDes = p.vigueur;
	}else if(strcmp(stat,"perception") == 0){
		nbDes = p.perception;
	}else if(strcmp(stat,"intelligence") == 0){
		nbDes = p.intelligence;
	}
	
	int nbSucces = 0;
	int jet;
	ecrireDansLog(l,"\U0001F3B2 Lancé de dés : ",'d');
	l->maxLigne = l->maxLigne+1;
	allerFinTexte(l);

	for(int i=0; i<nbDes; i++){
		jet = rand()%10+1;
		char str[10];
		sprintf(str,"%s \u2063",DES[jet-1]);
		ecrireDansLog(l,str,'s');
		if(jet >= difficulte){
			nbSucces++;
		}else if(jet == 1){
			nbSucces--;
		}
	}

	l->maxLigne = l->maxLigne-1;
	ecrireDansLog(l," -> ",'s');

	if(nbSucces == 1){
		ecrireDansLog(l,"1 dé validé",'f');
		return true;
	}else if(nbSucces > 1){
		char str[16];
		sprintf(str,"%d dés validés",nbSucces);
		ecrireDansLog(l,str,'f');
		return true;
	}else{
		ecrireDansLog(l,"échec",'f');
		return false;
	}
	
}


/**
 * Used to display a wall for the map view
 * @param p1 First point with its coordinates
 * @param p2 Second point with its coordinates
 * @param affichage Window view
 * @param current Indicate if the wall is one of the current room being displayed
 */
void afficherMur(Point p1, Point p2, int affichage[LARGEUR_MAP][HAUTEUR_MAP],bool current){

	int distanceX = p2.x - p1.x;
	int distanceY = p2.y - p1.y;
	int decalage = 0; // decalage
	int direction = 1;
	int directionDecallage = 1;
	int codeMur = (current?2:1);
	int variation = 0;

	if(abs(distanceY) > abs(distanceX)){
		if(distanceY<0){direction = -1;}
		if(distanceX != 0){
			variation = distanceY/distanceX;
			if(distanceX<0){directionDecallage = -1;}
		}
		for(int i=0; i<abs(distanceY); i++){
			if(variation != 0 && i != 0){
				if(i%abs(variation) == 0){
					affichage[p1.y+i*direction][p1.x+decalage*directionDecallage] = (affichage[p1.y+i*direction][p1.x+decalage*directionDecallage]==2?2:codeMur);
					decalage = decalage+1;
				}
			}
			affichage[p1.y+i*direction][p1.x+decalage*directionDecallage] = (affichage[p1.y+i*direction][p1.x+decalage*directionDecallage]==2?2:codeMur);
		}
	}else{
		if(distanceX<0){direction = -1;}
		if(distanceY != 0){
			variation = distanceX/distanceY;
			if(distanceY<0){directionDecallage = -1;}
		}
		for(int i=0; i<abs(distanceX); i++){
			if(variation != 0){
				if(i%abs(variation) == 0){
					affichage[p1.y+decalage*directionDecallage][p1.x+i*direction] = (affichage[p1.y+decalage*directionDecallage][p1.x+i*direction]==2?2:codeMur);
					decalage = decalage+1;
				}
			}
			affichage[p1.y+decalage*directionDecallage][p1.x+i*direction] = (affichage[p1.y+decalage*directionDecallage][p1.x+i*direction]==2?2:codeMur);
		}
	}
	
}


/**
 * Used to display a room
 * @param piece Room structure
 * @param x
 * @param affichage Map view
 * @param current Indicate if the room is the current one according to the step
 */
void afficherPiece(Piece piece, int x, int affichage[LARGEUR_MAP][HAUTEUR_MAP], bool current){
	for(int i=0; i<piece.nbPoints; i++){
		afficherMur(*piece.points[i],*piece.points[(i+1)%piece.nbPoints],affichage,current);
	}
}


/**
 * Used to display the map view
 * @param map Map structure
 * @param e Step structure
 */
void afficherMap(Map map, Etape e){

	Piece pieceEnCours;
	bool enCours;

	for(int x=0; x<LARGEUR_MAP; x++){ // affichage de la map
		for(int y=0; y<HAUTEUR_MAP; y++){
			map.affichage[x][y] = 0;
		}
	}

	for(int i=0; i<map.nbPieces; i++){
		if(map.pieces[i]->visible != 0){
			if(strcmp(e.idPiece,map.pieces[i]->idPiece) == 0){
				pieceEnCours = *(map.pieces[i]);
				enCours = true;
			}else{
				enCours = false;
			}
			afficherPiece(*(map.pieces[i]),i,map.affichage, enCours);
		}
	}

	int minX = LARGEUR_MAP, maxX = 0, maxY = 0, minY = HAUTEUR_MAP;
	for(int i=0; i<pieceEnCours.nbPoints; i++){
		minX = min(minX,pieceEnCours.points[i]->x);
		minY = min(minY,pieceEnCours.points[i]->y);
		maxX = max(maxX,pieceEnCours.points[i]->x);
		maxY = max(maxY,pieceEnCours.points[i]->y);
	}

	int decalageY = max(0,(LINES-15-minX-maxX)/2);
	int decalageX = COLS/2-(minY+maxY)/2;
	int haut, bas, gauche, droite;
	int debutX = 0;
	int finX = LARGEUR_MAP;
	int debutY = 0;
	int finY = min(HAUTEUR_MAP,LINES-16);

	// Display the map
	for(int x=debutX; x<finX; x++){
		for(int y=debutY; y<finY; y++){
			if(map.affichage[x][y] == 1 || map.affichage[x][y] == 2){ // il y a un mur
				if(x == 0){gauche = 0;}else{gauche = (map.affichage[x-1][y] != 0?1:0);}
				if(x == LARGEUR_MAP-1){droite = 0;}else{droite = (map.affichage[x+1][y] != 0?1:0);}
				if(y == 0){haut = 0;}else{haut = (map.affichage[x][y-1] != 0?1:0);}
				if(y == HAUTEUR_MAP-1){bas = 0;}else{bas = (map.affichage[x][y+1] != 0?1:0);}
				afficher(y-debutY+decalageY,x-debutX+decalageX,TAB_MURS[haut][bas][gauche][droite],(map.affichage[x][y] == 1?BLANC_SUR_NOIR:JAUNE_SUR_NOIR));
			}
		}
	}

    if(finY > decalageY+16){afficher(decalageY+16,decalageX+5,"\u2577",BRUN_SUR_NOIR);} // Door
    if(finY > decalageY+11){afficher(decalageY+11,decalageX+8,"\u2574",BRUN_SUR_NOIR);} // Door
    if(finY > decalageY+18){afficher(decalageY+18,decalageX+28,"\u2577",BRUN_SUR_NOIR);} // Door
    if(finY > decalageY+21){afficher(decalageY+21,decalageX+32,"\u2574",BRUN_SUR_NOIR);} // Door
    if(finY > decalageY+15){afficher(decalageY+15,decalageX+32,"\u2574",BRUN_SUR_NOIR);} // Door

    if(finY > decalageY+2){afficher(decalageY+2,decalageX+1,"\U0001F335",VERT_SUR_NOIR);} // Cactus
    if(finY > decalageY+8){afficher(decalageY+8,decalageX+2,"\U0001F335",VERT_SUR_NOIR);} // Cactus
    if(finY > decalageY+10){ afficher(decalageY+10,decalageX,"\U0001F335",VERT_SUR_NOIR);} // Cactus
    if(finY > decalageY+12){afficher(decalageY+12,decalageX+1,"\U0001F335",VERT_SUR_NOIR);} // Cactus
    if(finY > decalageY+14){afficher(decalageY+14,decalageX+3,"\U0001F335",VERT_SUR_NOIR);} // Cactus
    if(finY > decalageY+17){afficher(decalageY+17,decalageX,"\U0001F335",VERT_SUR_NOIR);} // Cactus
    if(finY > decalageY+21){afficher(decalageY+21,decalageX+2,"\U0001F335",VERT_SUR_NOIR);} // Cactus
    if(finY > decalageY+27){afficher(decalageY+27,decalageX+3,"\U0001F335",VERT_SUR_NOIR);} // Cactus

    if(finY > decalageY+20){afficher(decalageY+20,decalageX+44,"\U0001F4DA",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+10){afficher(decalageY+10,decalageX+40,"\U0001F56E",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+16){afficher(decalageY+16,decalageX+39,"\U0001F4D8",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+8){afficher(decalageY+8,decalageX+32,"\U0001F4DA",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+15){afficher(decalageY+15,decalageX+40,"\U0001F56E",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+24){afficher(decalageY+24,decalageX+48,"\U0001F4DA",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+7){afficher(decalageY+7,decalageX+32,"\U0001F4D8",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+20){afficher(decalageY+20,decalageX+38,"\U0001F4DA",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+18){afficher(decalageY+18,decalageX+42,"\U0001F56E",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+14){afficher(decalageY+14,decalageX+41,"\U0001F4DA",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+9){afficher(decalageY+9,decalageX+35,"\U0001F56E",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+13){afficher(decalageY+13,decalageX+45,"\U0001F4DA",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+23){afficher(decalageY+23,decalageX+35,"\U0001F4DA",GRIS_SUR_NOIR);} // Book
    if(finY > decalageY+17){afficher(decalageY+17,decalageX+38,"\U0001F4D8",GRIS_SUR_NOIR);} // Book
}


/**
 * Used to display character life
 * @param p Character structure
 */
void afficherVies(Personnage p){

	afficher(LINES-11,COLS-21,"Vie",JAUNE_SUR_NOIR);
	afficher(LINES-10,COLS-21,"Humanité",JAUNE_SUR_NOIR);
	afficher(LINES-9,COLS-21,"Dextérité",JAUNE_SUR_NOIR);
	afficher(LINES-8,COLS-21,"Vigueur",JAUNE_SUR_NOIR);
	afficher(LINES-7,COLS-21,"Perception",JAUNE_SUR_NOIR);
	afficher(LINES-6,COLS-21,"Intellig.",JAUNE_SUR_NOIR);

	for(int i=0; i<5;i++){
		afficher(LINES-11,COLS-10+i*2,"\u2665",(i<p.vie?BLANC_SUR_NOIR:GRIS_FONCE_SUR_NOIR));
		afficher(LINES-10,COLS-10+i*2,"\U0001F465",(i<p.humanite?BLANC_SUR_NOIR:GRIS_FONCE_SUR_NOIR));
		afficher(LINES-9,COLS-10+i*2,"\U0001F527",(i<p.dexterite?BLANC_SUR_NOIR:GRIS_FONCE_SUR_NOIR));
		afficher(LINES-8,COLS-10+i*2,"\U0001F4AA",(i<p.vigueur?BLANC_SUR_NOIR:GRIS_FONCE_SUR_NOIR));
		afficher(LINES-7,COLS-10+i*2,"\U0001F441",(i<p.perception?BLANC_SUR_NOIR:GRIS_FONCE_SUR_NOIR));
		afficher(LINES-6,COLS-10+i*2,"\U0001F4D8",(i<p.intelligence?BLANC_SUR_NOIR:GRIS_FONCE_SUR_NOIR));
	}

}


/**
 * Used to display the current time of the story
 */
void afficherTime(){
	int temps = atoi(getParemeterValueInSave("S.timer"));
	int heureDebut = (21*60+temps)%(24*60);
	int minutes = heureDebut%60;
	int heure = heureDebut/60;
	char affichage[6];
	affichage[0] = (heure/10)+'0';
	affichage[1] = (heure%10)+'0';
	affichage[2] = 'h';
	affichage[3] = (minutes/10)+'0';
	affichage[4] = (minutes%10)+'0';
	affichage[5] = '\0';
	afficher(LINES-13,COLS-13,affichage,BLANC_SUR_NOIR);
}


/**
 * Used to display the complete view of the game
 * @param p Character structure
 * @param l Log structure
 * @param m Map structure
 * @param e Step structure
 */
void afficherEcran(Personnage p,Log *l, Map m, Etape e){
	afficherVies(p);
	afficherMap(m, e);
	afficherTexte(l);
    afficherTime();
}


/**
 * Used to reinitialize the current view of the game
 * @param p Character structure
 * @param l Log structure
 * @param m Map structure
 * @param e Step structure
 */
void reinitialiserAffichage(Personnage p,Log *l, Map m, Etape e){
	clear();
	
	// Dividing line between MAP & TEXT
	for(int i=0;i<COLS;i++){
		afficher(LINES-16,i,"\u2500",BLANC_SUR_NOIR);
	}
	
	// Dividing line between TEXT & LIFE
	for(int i=LINES;i>LINES-16;i--){
		afficher(i,COLS-23,"\u2502",BLANC_SUR_NOIR);
	}
	
	// Dividing line between NAME, LIFE & HELP
	for(int i=22;i>0;i--){
		afficher(LINES-14,COLS-i,"\u2500",BLANC_SUR_NOIR);
		afficher(LINES-12,COLS-i,"\u2500",BLANC_SUR_NOIR);
		afficher(LINES-5,COLS-i,"\u2500",BLANC_SUR_NOIR);
	}
	
	afficher(LINES-16,COLS-23,"\u252C",BLANC_SUR_NOIR); // Intersection
	afficher(LINES-14,COLS-23,"\u251C",BLANC_SUR_NOIR); // Intersection
	afficher(LINES-12,COLS-23,"\u251C",BLANC_SUR_NOIR); // Intersection
	afficher(LINES-5,COLS-23,"\u251C",BLANC_SUR_NOIR); // Intersection
	afficher(LINES-13,COLS-21,"Heure",JAUNE_SUR_NOIR); // Timer
	afficher(LINES-15,COLS-21,p.nom,JAUNE_SUR_NOIR); // Vampire's name

	afficher(LINES-4,COLS-21,"Aide",BLANC_SUR_NOIR); // Help
	afficher(LINES-2,COLS-21,"Quitter",BLANC_SUR_NOIR);
	afficher(LINES-4,COLS-2,"H",JAUNE_SUR_NOIR);
	afficher(LINES-2,COLS-6,"ECHAP",JAUNE_SUR_NOIR);
	afficherEcran(p,l,m,e);

}


/**
 * Used to display the help menu
 */
void afficherAide(){

	int hauteurAide = 20;
	int largeurAide = 40;
	int decallageX = (COLS-largeurAide)/2;
	int decallageY = (LINES-hauteurAide)/2;
	
	for(int i=-2;i<=largeurAide+2;i++){
		for(int h=-2; h<=hauteurAide+2; h++){
			afficher(decallageY+h,decallageX+i," ",BLANC_SUR_NOIR);		
		}
	}
	
	for(int i=0;i<=largeurAide;i++){
		afficher(decallageY,decallageX+i,"X",BLANC_SUR_NOIR);
		afficher(decallageY+hauteurAide,decallageX+i,"X",BLANC_SUR_NOIR);
	}

	for(int i=0;i<hauteurAide;i++){
		afficher(decallageY+i,decallageX,"X",BLANC_SUR_NOIR);
		afficher(decallageY+i,decallageX+largeurAide,"X",BLANC_SUR_NOIR);
	}
	
	afficher(decallageY+2,decallageX+17," AIDE ",NOIR_SUR_BLANC);
	int afficherAides = decallageY+3; int afficherDescription = decallageX+2; int afficherRaccourci = decallageX+33;
	afficher(afficherAides+1,afficherDescription,"Choisir une option",BLANC_SUR_NOIR);
	afficher(afficherAides+2,afficherDescription,"Valider le choix de l'option",BLANC_SUR_NOIR);
	afficher(afficherAides+4,afficherDescription,"Afficher Aide",BLANC_SUR_NOIR);
	afficher(afficherAides+7,afficherDescription,"Texte précédent",BLANC_SUR_NOIR);
	afficher(afficherAides+8,afficherDescription,"Texte suivant",BLANC_SUR_NOIR);
	afficher(afficherAides+9,afficherDescription,"Aller à la fin du texte",BLANC_SUR_NOIR);
	afficher(afficherAides+11,afficherDescription,"Retourner au menu",BLANC_SUR_NOIR);
	afficher(afficherAides+12,afficherDescription,"Quitter le jeu",BLANC_SUR_NOIR);
	
	afficher(afficherAides+1,afficherRaccourci,"1-9",JAUNE_SUR_NOIR);
	afficher(afficherAides+2,afficherRaccourci,"ENTER",JAUNE_SUR_NOIR);
	afficher(afficherAides+4,afficherRaccourci,"H",JAUNE_SUR_NOIR);
	afficher(afficherAides+7,afficherRaccourci,"\u2B9D",JAUNE_SUR_NOIR);
	afficher(afficherAides+8,afficherRaccourci,"\u2B9F",JAUNE_SUR_NOIR);
	afficher(afficherAides+9,afficherRaccourci,"E",JAUNE_SUR_NOIR);
	afficher(afficherAides+11,afficherRaccourci,"M",JAUNE_SUR_NOIR);
	afficher(afficherAides+12,afficherRaccourci,"ECHAP",JAUNE_SUR_NOIR);
	afficher(decallageY+18,decallageX+8,"Appuyez sur une touche...",JAUNE_SUR_NOIR);
	afficher(LINES-1,COLS-1," ",BLANC_SUR_NOIR);

}


/**
 * Used to display the Game Over menu
 */
void afficherGameOver(){

	// Delete save
	remove("xml/saves/save.xml");

	int hauteur = 21;
	int largeur = 40;
	int decallageX = (COLS-largeur)/2;
	int decallageY = (LINES-hauteur)/2;
	clear();

	for(int i=-2;i<=largeur+2;i++){
		for(int h=-1; h<=hauteur+1; h++){
			afficher(decallageY+h,decallageX+i," ",NOIR_SUR_BLANC);		
		}
	}
	
	for(int i=0;i<=largeur;i++){
		afficher(decallageY,decallageX+i,"X",NOIR_SUR_BLANC);
		afficher(decallageY+hauteur,decallageX+i,"X",NOIR_SUR_BLANC);
	}

	for(int i=0;i<hauteur;i++){
		afficher(decallageY+i,decallageX,"X",NOIR_SUR_BLANC);
		afficher(decallageY+i,decallageX+largeur,"X",NOIR_SUR_BLANC);
	}

	int positionYGame = decallageY+2;
	int positionYOver = decallageY+11;
	int positionX = decallageX+8;

	afficher(decallageY+19,decallageX+7," Appuyez sur une touche... ",ROUGE_SUR_BLANC);
	afficher(positionYGame,positionX,"\u25A0\u25A0\u25A0\u25A0\u25A0  \u25A0\u25A0\u25A0  \u25A0\u25A0   \u25A0 \u25A0\u25A0\u25A0\u25A0\u25A0",ROUGE_SUR_BLANC);
	afficher(positionYGame+1,positionX,"\u25A0\u25A0     \u25A0\u25A0\u25A0  \u25A0\u25A0\u25A0 \u25A0\u25A0 \u25A0\u25A0",ROUGE_SUR_BLANC);
	afficher(positionYGame+2,positionX,"\u25A0\u25A0    \u25A0\u25A0  \u25A0 \u25A0\u25A0 \u25A0 \u25A0 \u25A0\u25A0",ROUGE_SUR_BLANC);
	afficher(positionYGame+3,positionX,"\u25A0\u25A0    \u25A0\u25A0  \u25A0 \u25A0\u25A0   \u25A0 \u25A0\u25A0\u25A0\u25A0\u25A0",ROUGE_SUR_BLANC);
	afficher(positionYGame+4,positionX,"\u25A0\u25A0 \u25A0\u25A0 \u25A0\u25A0\u25A0\u25A0\u25A0 \u25A0\u25A0   \u25A0 \u25A0\u25A0",ROUGE_SUR_BLANC);
	afficher(positionYGame+5,positionX,"\u25A0\u25A0  \u25A0 \u25A0\u25A0  \u25A0 \u25A0\u25A0   \u25A0 \u25A0\u25A0",ROUGE_SUR_BLANC);
	afficher(positionYGame+6,positionX,"\u25A0\u25A0\u25A0\u25A0\u25A0 \u25A0\u25A0  \u25A0 \u25A0\u25A0   \u25A0 \u25A0\u25A0\u25A0\u25A0\u25A0",ROUGE_SUR_BLANC);
	
	afficher(positionYOver,positionX," \u25A0\u25A0\u25A0  \u25A0\u25A0  \u25A0 \u25A0\u25A0\u25A0\u25A0\u25A0 \u25A0\u25A0\u25A0\u25A0",ROUGE_SUR_BLANC);
	afficher(positionYOver+1,positionX,"\u25A0\u25A0 \u25A0\u25A0 \u25A0\u25A0  \u25A0 \u25A0\u25A0    \u25A0\u25A0  \u25A0",ROUGE_SUR_BLANC);
	afficher(positionYOver+2,positionX,"\u25A0\u25A0  \u25A0 \u25A0\u25A0  \u25A0 \u25A0\u25A0    \u25A0\u25A0  \u25A0",ROUGE_SUR_BLANC);
	afficher(positionYOver+3,positionX,"\u25A0\u25A0  \u25A0 \u25A0\u25A0  \u25A0 \u25A0\u25A0\u25A0\u25A0  \u25A0\u25A0 \u25A0",ROUGE_SUR_BLANC);
	afficher(positionYOver+4,positionX,"\u25A0\u25A0  \u25A0 \u25A0\u25A0  \u25A0 \u25A0\u25A0    \u25A0\u25A0\u25A0",ROUGE_SUR_BLANC);
	afficher(positionYOver+5,positionX,"\u25A0\u25A0  \u25A0 \u25A0\u25A0 \u25A0  \u25A0\u25A0    \u25A0\u25A0 \u25A0",ROUGE_SUR_BLANC);
	afficher(positionYOver+6,positionX," \u25A0\u25A0\u25A0   \u25A0\u25A0\u25A0  \u25A0\u25A0\u25A0\u25A0\u25A0 \u25A0\u25A0  \u25A0",ROUGE_SUR_BLANC);
	afficher(LINES-1,COLS-1," ",BLANC_SUR_NOIR);

}

void gererJeu();
void afficherMenu();


/**
 * Used to display the manu to create a character
 */
void afficherCreationPersonnage(){

	int hauteur = 21;
	int largeur = 40;
	int decallageX = (COLS-largeur)/2;
	int decallageY = (LINES-hauteur)/2;
	clear();

	for(int i=-2;i<=largeur+2;i++){
		for(int h=-1; h<=hauteur+1; h++){
			afficher(decallageY+h,decallageX+i," ",NOIR_SUR_BLANC);
		}
	}

	for(int i=0;i<=largeur;i++){
		afficher(decallageY,decallageX+i,"X",NOIR_SUR_BLANC);
		afficher(decallageY+hauteur,decallageX+i,"X",NOIR_SUR_BLANC);
	}

	for(int i=0;i<hauteur;i++){
		afficher(decallageY+i,decallageX,"X",NOIR_SUR_BLANC);
		afficher(decallageY+i,decallageX+largeur,"X",NOIR_SUR_BLANC);
	}

	afficher(decallageY+2,decallageX+9," Création du personnage ",ROUGE_SUR_BLANC);
	afficher(decallageY+5,decallageX+6,"Nom",NOIR_SUR_BLANC);
	afficher(decallageY+6,decallageX+6,"Dextérité",NOIR_SUR_BLANC);
	afficher(decallageY+7,decallageX+6,"Vigueur",NOIR_SUR_BLANC);
	afficher(decallageY+8,decallageX+6,"Perception",NOIR_SUR_BLANC);
	afficher(decallageY+9,decallageX+6,"Intelligence",NOIR_SUR_BLANC);
	afficher(decallageY+12,decallageX+6,"Points restants :",NOIR_SUR_BLANC);

	afficher(decallageY+16,decallageX+6,"Valider",GRIS_SUR_BLANC);
	afficher(decallageY+18,decallageX+6,"Retour au menu",NOIR_SUR_BLANC);
	afficher(decallageY+19,decallageX+6,"Quitter",NOIR_SUR_BLANC);
	int posReponse = decallageX+25;

	for(int h=6;h<=9;h++){
		afficher(decallageY+h,posReponse,"\u25A3",NOIR_SUR_BLANC);
		for(int i=1; i<5; i++){
			afficher(decallageY+h,posReponse+i*2,"\u25A1",NOIR_SUR_BLANC);
		}
	}

	for(int i=0; i<10; i++){
		afficher(decallageY+5,posReponse+i,"_",BLANC_SUR_NOIR);
	}

	char strPoint[5];
	int position = 0, positionPrec = 0;
	int pointsRestant = 7;
	int valeurs[5] = {1,1,1,1,1};
	sprintf(strPoint,"%d",pointsRestant);
	afficher(decallageY+12,decallageX+24,strPoint,VERT_SUR_BLANC);
	Personnage *p = initPersonnage("");

	afficher(decallageY+5+position,decallageX+4,"\u27A7",NOIR_SUR_BLANC);

    bool validePossible = false;
	bool fin = false;
	int input = -1;

    while(!fin){
		afficher(LINES-1,COLS-1," ",BLANC_SUR_NOIR);
		input = getch();
		switch(input){
			case KEY_RESIZE:
				if(COLS < MIN_COLS || LINES < MIN_LINES){
					clear();
					char str[50];
					sprintf(str,"Dimensions actuelles : %dx%d",COLS,LINES);
					mvprintw(1,1,"ERREUR : écran trop petit !");
					mvprintw(2,1,str);
					mvprintw(3,1,"Dimensions minimales : %dx%d",MIN_COLS,MIN_LINES);
				}else{
					afficherCreationPersonnage();
				}
        		break;
			case KEY_UP:
				positionPrec = position;
				position--;
				if(position == -1){position = 14;}
				else if(position == 10){position = 4;}
				else if(position == 12){if(validePossible){position = 11;}else{position = 4;}}
				break;
			case KEY_DOWN:
				positionPrec = position;
				position++;
				if(position == 5){if(validePossible){position = 11;}else{position = 13;};}
				else if(position == 12){position = 13;}
				else if(position == 15){position = 0;}
				break;
			case KEY_LEFT:
				if(position > 0 && position < 6){
					if(valeurs[position-1] > 1){
						valeurs[position-1]--;
						pointsRestant++;
					}
				}
				break;
			case KEY_RIGHT:
				if(position > 0 && position < 6){
					if(valeurs[position-1] < 5 && pointsRestant > 0){
						valeurs[position-1]++;
						pointsRestant--;
					}
				}
				break;
			case 10: // Enter
				if(position == 11 || position == 13 || position == 14){
					fin = true;
				}
				break;
			case 27: // Exit -> Back to the menu
				fin = true;
				break;
			default: // Text
				if(position == 0){ // Name field
					int nbChars = strlen(p->nom);
					if(nbChars < 10 && ((input >= 64 && input <= 90) || (input >= 97 && input <= 122))){
						p->nom[nbChars] = (char)input;
					}else if(nbChars > 0 && (input == 263 || input == 330)){
						p->nom[nbChars-1] = '\0';
					}
				}
			}

			validePossible = (pointsRestant == 0 && strlen(p->nom) != 0);
			afficher(decallageY+16,decallageX+6,"Valider",(validePossible?NOIR_SUR_BLANC:GRIS_SUR_BLANC));

			if(position > 0 && position < 6){
				for(int i=0;i<valeurs[position-1];i++){
					afficher(decallageY+5+position,posReponse+i*2,"\u25A3",NOIR_SUR_BLANC); // Full squares
				}
				for(int i=valeurs[position-1];i<5;i++){
					afficher(decallageY+5+position,posReponse+i*2,"\u25A1",NOIR_SUR_BLANC); // Empty squares
				}
				afficher(decallageY+5+position,posReponse-2,"\u2B9C",NOIR_SUR_BLANC); // Arrows
				afficher(decallageY+5+position,posReponse+10,"\u2B9E",NOIR_SUR_BLANC);
				sprintf(strPoint,"%d",pointsRestant);
				afficher(decallageY+12,decallageX+24,"    ",NOIR_SUR_BLANC);
				afficher(decallageY+12,decallageX+24,strPoint,(pointsRestant>0?VERT_SUR_BLANC:ROUGE_SUR_BLANC));
			}else if(position == 0){
				afficher(decallageY+5,posReponse,p->nom,BLANC_SUR_NOIR);
				for(int i=strlen(p->nom); i<10; i++){
					afficher(decallageY+5,posReponse+i,".",BLANC_SUR_NOIR);
				}
			}

			if(positionPrec > 0 && positionPrec < 8){
				afficher(decallageY+5+positionPrec,posReponse-2," ",NOIR_SUR_BLANC);
				afficher(decallageY+5+positionPrec,posReponse+10," ",NOIR_SUR_BLANC);
			}else if(positionPrec == 0){
				afficher(decallageY+5,posReponse,p->nom,NOIR_SUR_BLANC);
				for(int i=strlen(p->nom); i<10; i++){
					afficher(decallageY+5,posReponse+i,"_",NOIR_SUR_BLANC);
				}
			}

			afficher(decallageY+5+positionPrec,decallageX+4," ",NOIR_SUR_BLANC);
			afficher(decallageY+5+position,decallageX+4,"\u27A7",NOIR_SUR_BLANC); // affichage de la position actuelle
	}

    if(position == 11){
        saveData(
                (char *[]){"P.nom", "P.vie", "P.humanite", "P.dexterite", "P.vigueur", "P.perception", "P.intelligence"},
                (char *[]){p->nom, "5", "5", intToStr(valeurs[0]), intToStr(valeurs[1]), intToStr(valeurs[2]), intToStr(valeurs[3])},
                7
        );
        saveData(
                (char *[]){"S.id","S.timer","O.parchemin"},
                (char *[]){"1","0","false"},
                3
        );
        gererJeu();
    }else if(position == 13 || input == 27){
        afficherMenu();
    }

}


/**
 * Used to clear the log file
 */
void clearLogFile() {
	fclose(fopen("log.txt", "w"));
	Log *l = initLogFile();
	ecrireDansLog(l, "Bienvenue dans ce jeu où vous êtes le héros !",'h');
}


/**
 * Used to display the main menu
 */
void afficherMenu(){

	int hauteur = 21;
	int largeur = 40;
	int decallageX = (COLS-largeur)/2;
	int decallageY = (LINES-hauteur)/2;
	clear();
	
	for(int i=-2;i<=largeur+2;i++){
		for(int h=-1; h<=hauteur+1; h++){
			afficher(decallageY+h,decallageX+i," ",NOIR_SUR_BLANC);		
		}
	}
	
	for(int i=0;i<=largeur;i++){
		afficher(decallageY,decallageX+i,"X",NOIR_SUR_BLANC);
		afficher(decallageY+hauteur,decallageX+i,"X",NOIR_SUR_BLANC);
	}

	for(int i=0;i<hauteur;i++){
		afficher(decallageY+i,decallageX,"X",NOIR_SUR_BLANC);
		afficher(decallageY+i,decallageX+largeur,"X",NOIR_SUR_BLANC);
	}
	
	bool sauvegardeExistante = access("xml/saves/save.xml",F_OK) != -1;
	afficher(decallageY+2,decallageX+12," Vampire's Library ",ROUGE_SUR_BLANC);
	afficher(decallageY+5,decallageX+6,"Nouvelle partie",NOIR_SUR_BLANC);
	afficher(decallageY+6,decallageX+6,"Reprendre partie existante",(sauvegardeExistante?NOIR_SUR_BLANC:GRIS_SUR_BLANC));
	afficher(decallageY+9,decallageX+6,"Quitter",NOIR_SUR_BLANC);
	
	int nom = decallageY+14;
	afficher(nom,decallageX+4,"Jeu réalisé par :",NOIR_SUR_BLANC);
	afficher(nom+1,decallageX+5,"\u21AC Ophélien AMSLER",NOIR_SUR_BLANC);
	afficher(nom+2,decallageX+5,"\u21F4 Pierrick MASSIN",NOIR_SUR_BLANC);
	afficher(nom+3,decallageX+5,"\u21DD Bertrand MULLER",NOIR_SUR_BLANC);
	afficher(nom+5,decallageX+11,"Projet C-SHELL 2018",NOIR_SUR_BLANC);
	int position = 0;
	
	afficher(decallageY+5+position,decallageX+4,"\u27A7",NOIR_SUR_BLANC);
	afficher(LINES-1,COLS-1," ",BLANC_SUR_NOIR);

    if(COLS < MIN_COLS || LINES < MIN_LINES){
        clear();
        char str[50];
        sprintf(str,"Dimensions actuelles : %dx%d",COLS,LINES);
        mvprintw(1,1,"ERREUR : écran trop petit !");
        mvprintw(2,1,str);
        mvprintw(3,1,"Dimensions minimales : %dx%d",MIN_COLS,MIN_LINES);
    }

	int input = -1;
	while(input != 10 && input != 27){ // entrer
		afficher(LINES-1,COLS-1," ",BLANC_SUR_NOIR);
		input = getch();
		switch(input){
			case KEY_RESIZE:
				if(COLS < MIN_COLS || LINES < MIN_LINES){
					clear();
					char str[50];
					sprintf(str,"Dimensions actuelles : %dx%d",COLS,LINES);
					mvprintw(1,1,"ERREUR : écran trop petit !");
					mvprintw(2,1,str);
					mvprintw(3,1,"Dimensions minimales : %dx%d",MIN_COLS,MIN_LINES);
				}else{
					afficherMenu();
				}
        		break;
			case KEY_UP: 
				afficher(decallageY+5+position,decallageX+4," ",NOIR_SUR_BLANC);
				position--;
				if(position == -1){position = 4;}
				else if(position == 3){position = (sauvegardeExistante?1:0);}
				afficher(decallageY+5+position,decallageX+4,"\u27A7",NOIR_SUR_BLANC);
				break;
			case KEY_DOWN: 
				afficher(decallageY+5+position,decallageX+4," ",NOIR_SUR_BLANC);
				position++;
				if((position == 2 && sauvegardeExistante) || (position == 1 && !sauvegardeExistante)){position = 4;}
				else if(position == 5){position = 0;}
				afficher(decallageY+5+position,decallageX+4,"\u27A7",NOIR_SUR_BLANC);
				break;
		}
	}

	if(input == 10){
		switch(position){
			case 0: // New game
	         clearLogFile();
				afficherCreationPersonnage();
				break;
			case 1: // Load game
				gererJeu();
				break;
			case 4: // Exit
				saveData((char *[]){"LastPlay"}, (char *[]){getTime()}, 1);
				break;
		}
	}

}


/**
 * Used to know if an introduction has already been displayed
 * @param l Log structure
 * @return Boolean to know if introduction already exists
 */
bool introductionDejaAffichee(Log *l) {
	return (l->maxLigne > 2);
}


/**
 * Used to store the current sotry (step)
 * @param l Log structure
 * @param e Step structure
 */
void writeStoryStepAndOptions(Log *l, Etape *e) {
	ecrireDansLog(l, e->texte,'h');
	for(int i = 0; i < e->nbOptions; i++) {
		ecrireDansLog(l, e->options[i]->titre,'t');
		ecrireDansLog(l, e->options[i]->texte,'o');
	}
	l->afficherLigneEntiere = 1;
	if(e->nbOptions < 2){
		l->ligneActuelle++; 
	}else{
		l->ligneActuelle += 3;
	}
}


/**
 * Used to manage the game
 */
void gererJeu(){
	
	// Initialize the game
	Log *l = initLogFile();
	Personnage *p = initPersonnageFromSave();
    Etape *e = initEtapeFromSave();
    Map *m = initMap();
    loadStep(e, e->idPiece);
    loadMap("map", m);
    
	// Initialize log
	if(!introductionDejaAffichee(l)) {
		writeStoryStepAndOptions(l, e);
	}

	int input = 0;
	bool fin = false, gameOver = false, victoire = false;

	// Reinitialize display
	reinitialiserAffichage(*p, l, *m,*e);
	
	// Detect inputs
	char str[31];
	int etapeSelectionnee = -1;
	while(!fin)
	{
		mvprintw(LINES-1,COLS-1," ",NOIR_SUR_BLANC);
		input = getch();
		switch(input)
		{
			case KEY_RESIZE:
				if(COLS < MIN_COLS || LINES < MIN_LINES){
					clear();
					sprintf(str,"Dimensions actuelles : %dx%d",COLS,LINES);
					mvprintw(1,1,"ERREUR : écran trop petit !");
					mvprintw(2,1,str);
					mvprintw(3,1,"Dimensions minimales : %dx%d",MIN_COLS,MIN_LINES);
				}else{
					l->afficherLigneEntiere = 1;
         	    reinitialiserAffichage(*p,l,*m,*e);
             }
        		break;
			case KEY_UP: 
				if(l->ligneActuelle >= 1){
					if(l->rangLigneActuelle > 0){
						l->rangAAjouter = -1;		
					}else if(l->ligneActuelle > 1){
						l->ligneActuelle = l->ligneActuelle-1;
						l->afficherLigneEntiere = 1;
					}
					afficherTexte(l);
				}
				break;
			case KEY_DOWN: 
				if(l->ligneActuelle <= l->maxLigne){
					if(l->rangLigneActuelle < l->rangLigneMax){
						l->rangAAjouter = 1;			
					}else if(l->ligneActuelle < l->maxLigne){
						l->ligneActuelle = l->ligneActuelle+1;
						l->rangLigneActuelle = 0;
					}
					afficherTexte(l);
				}
				break;
			case 'h':
			case 'H':
				afficherAide();
				input = getch();
           	    reinitialiserAffichage(*p,l,*m,*e);
				break;
			case 'E': 
			case 'e':
				allerFinTexte(l);
				break;
			case 'M':
			case 'm':
        	    afficherMenu();
				fin = true;
				break;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				if(l->nouveautes == 0){
					etapeSelectionnee = input-'0';
					if(etapeSelectionnee >= 1 && etapeSelectionnee <= e->nbOptions){
						char str[63];
						sprintf(str," Option %d choisie - Appuyez sur ENTRER pour valider ",etapeSelectionnee);
						afficherInformation(str,STATUS_INFO);
					}else{
						afficherInformation(" Cette option n'existe pas ",STATUS_ERREUR);
						etapeSelectionnee = -1;
					}
				}else{
					afficherInformation(" Vous n'avez pas encore lu tout le texte ",STATUS_TODO);
				}
				break;
			case 10: // Enter

				if(etapeSelectionnee == -1){
					afficherInformation(" Vous devez choisir une option ",STATUS_ERREUR);
				}else if(etapeSelectionnee >= 1 && etapeSelectionnee <= e->nbOptions){

                    e->nbOptions = 0;
					Action *a = getAction(e->options[etapeSelectionnee-1]->action);
					bool reussite, fini = false;

					if(strcmp(a->nomMethode,"noAction") == 0){
						reussite = true;
					}else if(strcmp(a->nomMethode,"jetDeDes") == 0){
						if(a->nbParametres == 2){
							reussite = jetDeDes(a->parametres[0],atoi(a->parametres[1]),l,*p);
						}else{
							reussite = true;
						}
					}else if(strcmp(a->nomMethode,"verifierParchemin") == 0){
						reussite = (strcmp(getParemeterValueInSave("O.parchemin"),"true") == 0);
					}else if(strcmp(a->nomMethode,"gameOver") == 0){
						fin = true;
						fini = true;
						gameOver = true;
						ecrireDansLog(l, "Vous avez PERDU !", 'g');
						ecrireDansLog(l, "Appuyez sur entrer pour quitter", 'h');
					}else if(strcmp(a->nomMethode,"victoire") == 0){
						fin = true;
						fini = true;
						ecrireDansLog(l, "Vous avez GAGNÉ !", 'v');
						ecrireDansLog(l, "Appuyez sur entrer pour quitter", 'h');
						victoire = true;
					}else{
						reussite = true;
					}

                    if(!fini){
						char *prochaineEtape, *texteAAfficher;
						int dureeProchaineEtape, humaniteARetirer, vieARetirer;

						if(reussite){
							dureeProchaineEtape = e->options[etapeSelectionnee-1]->dureeSucces;
							texteAAfficher = e->options[etapeSelectionnee-1]->reussite;
							prochaineEtape = e->options[etapeSelectionnee-1]->proEtapeSucces;
							humaniteARetirer = e->options[etapeSelectionnee-1]->humaniteSucces;
							vieARetirer = e->options[etapeSelectionnee-1]->vieSucces;

							if(strcmp(prochaineEtape,"14") == 0 && etapeSelectionnee-1 == 0){
								saveData(
										(char *[]){"O.parchemin"},
										(char *[]) {"true"},
										1
								);
							}
						}else{ // Fail
							dureeProchaineEtape = e->options[etapeSelectionnee-1]->dureeEchec;
							texteAAfficher = e->options[etapeSelectionnee-1]->echec;
							prochaineEtape = e->options[etapeSelectionnee-1]->proEtapeEchec;
							humaniteARetirer = e->options[etapeSelectionnee-1]->humaniteEchec;
							vieARetirer = e->options[etapeSelectionnee-1]->vieEchec;
						}

						ecrireDansLog(l, texteAAfficher, (reussite?'r':'e'));
						loadStep(e, prochaineEtape);
		                int temps = atoi(getParemeterValueInSave("S.timer"));
		                char timer[7];
						sprintf(timer,"%d",temps+dureeProchaineEtape);
						p->vie -= vieARetirer;
						p->humanite -= humaniteARetirer;
						char vie[2],humanite[2];
						sprintf(vie,"%d",p->vie);
						sprintf(humanite,"%d",p->humanite);

						saveData(
							(char *[]){"S.id","S.timer","P.vie","P.humanite"},
							(char *[]) {prochaineEtape,timer,vie,humanite},
							4
						);

						if(temps+dureeProchaineEtape > 600){
							fin = true;
							gameOver = true;
							ecrireDansLog(l, "Le jour se lève, vous avez PERDU !", 'g');
							ecrireDansLog(l, "Appuyez sur entrer pour quitter", 's');
						}

						if(p->vie <= 0){
							p->vie = 0;
							fin = true;
							gameOver = true;
							ecrireDansLog(l, "Vous n'avez plus de vie, vous avez PERDU !", 'g');
							ecrireDansLog(l, "Appuyez sur entrer pour quitter", 'h');
						}

						if(!gameOver){writeStoryStepAndOptions(l, e);}
						reinitialiserAffichage(*p,l,*m,*e);
						etapeSelectionnee = -1;

					}

				}else{
					afficherInformation(" Cette option n'existe pas ",STATUS_ERREUR);
					etapeSelectionnee = -1;
				}
				break;
			case 27: // Esc
				fin = true;
				break;
			default:
				break;
		}
	}

	if(gameOver || victoire){
		allerFinTexte(l);
		input = 1;
		while(input != 10){
			input = getch();
		}
		if(gameOver){
            afficherGameOver();
            input = getch();
        }else{
            remove("xml/saves/save.xml");
        }
     	afficherMenu();
	}

	// Free all the data used for the game
	free(m);
	free(e);
	free(p);
	free(l);

}


/**
 * Used to initialize the colors of the game
 */
void initialiserLesCouleurs(){
	init_color(100,572,286,176); // BRUN
	init_color(101,0,815,830); // TURQUOISE
	init_color(102,500,500,500); // GRIS
	init_color(104,700,700,700); // GRIS CLAIRE
	init_color(103,0,350,0); // VERT
	init_color(105,0,750,0); // VERT CLAIRE
	init_color(106,1000,150,150); // ROUGE CLAIRE
	init_color(107,300,300,300); // GRIS FONCE
	init_pair(BLANC_SUR_NOIR, COLOR_WHITE, COLOR_BLACK);
	init_pair(NOIR_SUR_BLANC, COLOR_BLACK, COLOR_WHITE);
	init_pair(GRIS_SUR_BLANC, 102, COLOR_WHITE);
	init_pair(GRIS_SUR_NOIR, 104, COLOR_BLACK);
	init_pair(JAUNE_SUR_NOIR, COLOR_YELLOW, COLOR_BLACK);
	init_pair(BLANC_SUR_ROUGE, COLOR_WHITE, COLOR_RED);
	init_pair(NOIR_SUR_TURQUOISE, COLOR_BLACK, 101);
	init_pair(BRUN_SUR_NOIR, 100, COLOR_BLACK);
	init_pair(ROUGE_SUR_BLANC, COLOR_RED, COLOR_WHITE);
	init_pair(ROUGE_SUR_NOIR, 106, COLOR_BLACK);
	init_pair(VERT_SUR_BLANC, 103, COLOR_WHITE);
	init_pair(VERT_SUR_NOIR, 105, COLOR_BLACK);
	init_pair(NOIR_SUR_GRIS, COLOR_BLACK, 104);
	init_pair(GRIS_FONCE_SUR_NOIR, 107, COLOR_BLACK);
}


/**
 * Mai program
 * @return Return code status
 */
int main(){

    // Unicode chars
    setlocale(LC_CTYPE,"fr_FR.UTF-8");
    setlocale(LC_NUMERIC,"fr_FR.UTF-8");
    setlocale(LC_TIME,"fr_FR.UTF-8");
    setlocale(LC_COLLATE,"fr_FR.UTF-8");
    setlocale(LC_MONETARY,"fr_FR.UTF-8");
    setlocale(LC_MESSAGES,"fr_FR.UTF-8");
    setlocale(LC_PAPER,"fr_FR.UTF-8");
    setlocale(LC_NAME,"fr_FR.UTF-8");
    setlocale(LC_ADDRESS,"fr_FR.UTF-8");
    setlocale(LC_TELEPHONE,"fr_FR.UTF-8");
    setlocale(LC_MEASUREMENT,"fr_FR.UTF-8");
    setlocale(LC_IDENTIFICATION,"fr_FR.UTF-8");
    setlocale(LC_ALL,"");

	// Curses Initialisations 
	initscr(); // New game window
	start_color(); // Colors available
	initialiserLesCouleurs();
	setvbuf(stdout, NULL, _IOLBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
  
	keypad(stdscr, TRUE); noecho(); // Doesn't display user inputs on screen
	afficherMenu();
    getch();
	endwin(); // Close game window
	return 0;

}
