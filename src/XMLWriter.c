#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "XMLWriter.h"


/**
 * Used to create a save file if it doesn't exist
 */
void createSaveFile() {
    FILE *save = fopen("xml/saves/save.xml", "w+");
    if(save) {
        fputs("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", save);
        fputs("<Save></Save>", save);
    }
    fclose(save);
}


/**
 * Used to save data in order to reload them next time the user will play the game
 * @param dataTitles Labels of the data to set
 * @param dataValues Values of the data to set
 * @param numberOfData Number of data to set
 */
void saveData(char *dataTitles[], char *dataValues[], int numberOfData) {

    xmlDocPtr saveFile;
    xmlXPathContextPtr pathContextPtr;
    xmlXPathObjectPtr objPtr;
    xmlNodePtr newNode, rootNode;

    // Check if file for save exists
    char *fileName = "xml/saves/save.xml";
    saveFile = xmlParseFile(fileName);
    if(saveFile == NULL) {
        createSaveFile();
        saveFile = xmlParseFile(fileName);
    }

    // Get XPath context
    pathContextPtr = xmlXPathNewContext(saveFile);
    if(pathContextPtr == NULL) {
        printf("Erreur. Impossible de créer le contexte pour le fichier '%s'.\n", fileName);
        xmlFreeDoc(saveFile);
        exit(127);
    }

    // Save data one by one
    for(int i = 0; i < numberOfData; i++) {

        char *dataPath = malloc(strlen("/Save/") + strlen(dataTitles[i]) + 1);
        strcpy(dataPath, "/Save/");
        strcat(dataPath, dataTitles[i]);
        objPtr = xmlXPathEvalExpression((xmlChar *) dataPath, pathContextPtr);

        // Create the node if it doesn't exist
        if(objPtr->nodesetval->nodeTab == NULL) {
            rootNode = xmlDocGetRootElement(saveFile);
            newNode = xmlNewChild(rootNode, NULL, (xmlChar *) dataTitles[i], (xmlChar *) dataValues[i]);
            xmlAddChild(rootNode, newNode);
            xmlSaveFileEnc(fileName, saveFile, "UTF-8");
            objPtr = xmlXPathEvalExpression((xmlChar *) dataPath, pathContextPtr);
        }

        xmlNode *node = objPtr->nodesetval->nodeTab[0];
        xmlNodeSetContent(node, (xmlChar *) dataValues[i]);
        xmlXPathFreeObject(objPtr);

    }

    // Free context
    xmlXPathFreeContext(pathContextPtr);

    // Save file
    xmlSaveFileEnc(fileName, saveFile, "UTF-8");
    xmlFreeDoc(saveFile);

}