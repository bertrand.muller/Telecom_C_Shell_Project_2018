#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "toolbox.h"


/**
 * Used to convert an int to a string
 * @param nb Number to convert as a string
 * @return String for that number
 */
char* intToStr(int nb) {
    char *str = malloc(15 * sizeof(char));
    sprintf(str, "%d", nb);
    return str;
}


/**
 * Used to get the current time
 * @return Current time
 */
char* getTime() {
    char *date = (char *) malloc(50 * sizeof(char));
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    strftime(date, sizeof(date)-1, "%d/%m/%Y %H:%M", t);
    printf("date = %s", date);
    return date;
}