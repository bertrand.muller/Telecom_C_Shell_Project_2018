# Rogue Like

C-Shell Project - March 2018

Authors: Ophélien Amsler, Pierrick Massin & Bertrand Müller

Télécom Nancy - Université de Lorraine

## Prerequisites

To be able to launch the game, you need to install three extra packages on your system.

    sudo apt-get install libxml2-dev libncursesw5-dev libncurses5-dev

## Compilation on a UNIX System

Compile the game files with the command below.

	make
	
## Launch of the game

To be able to play the game, you have to be at the root of the project and to execute the command below.

    ./Rocket