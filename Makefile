CC = gcc-5
CFLAGS = -Wall
DIR_LIBXML2 = -I/usr/include/libxml2
LIBS = -lxml2 -lncursesw
PROGRAM := Rocket
SRC := $(wildcard src/*.c)
OBJ := $(SRC:.c=.o)
DEST := .

$(DEST)/$(PROGRAM): $(OBJ)
	$(CC) $(DIR_LIBXML2) $(CFLAGS)  -o $@ $^ $(LIBS)
	mkdir -p xml/saves

%.o: %.c
	$(CC) $(DIR_LIBXML2) $(CFLAGS)  -o $@ -c $< $(LIBS)

.PHONY: clean mrproper

mrproper: clean
	rm -rf src/*.o

clean:
	rm -rf $(OBJ)

